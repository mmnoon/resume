import React from 'react';
import ReactWOW from 'react-wow';
import SingleExperience from './singleExperience'
import './stylesheets/reset.css';
import './stylesheets/experience.css';

export default function Experience(){
    return(
        <div className="container">
            <div className="exp-cont d-flex">

                <SingleExperience 
                 loadingDelay = {"0s"} 
                 additionalClass = {"first"}
                 jobRole = {"Android & Game Development"} 
                 companyName = {"Wangard International"} 
                 duration ={"March,2019 - Present"} 
                 jobDescription1 = {"Collaborated with UI and UX team to make application user friendly"}
                 jobDescription2 = {"Interfaced with internal/external mobile development teams from a technical and design perspective"}
                 jobDescription3 = {"Contributed to the design and development of mobile software libraries, tools and applications"}
                 />

                <SingleExperience 
                 loadingDelay = {"0.5s"}
                 additionalClass = {"second"}
                 jobRole = {"System Administrator"} 
                 companyName = {"MirindaWeb"} 
                 duration ={"March,2018 - March,2019"} 
                 jobDescription1 = {" Day-to-day system administration, including user security admin and role centre admin "}
                 jobDescription2 = {" Windows Server administration, systems documentation, backup configuration, systems performance monitoring, systems availability monitoring "}
                 jobDescription3 = {" Create, modify and delete users and user properties and network shares "}
                 />

                <SingleExperience 
                 loadingDelay = {"1s"} 
                 additionalClass = {"third"}
                 jobRole = {"System Administrator"} 
                 companyName = {"MicoTechX"} 
                 duration ={"Jan,2017 - Jan,2018"} 
                 jobDescription1 = {"Configuring and maintaining the networked computer system, including hardware, system software, and applications."}
                 jobDescription2 = {"Ensuring data is stored securely and backed up regularly"}
                 jobDescription3 = {"Diagnosing and resolving hardware, software, networking, and system issues when they arise."}
                 />

                <SingleExperience 
                 loadingDelay = {"1.5s"} 
                 additionalClass = {"fourth"}
                 jobRole = {"Intern"} 
                 companyName = {"Cecraba Technologies"} 
                 duration ={"Aug,2016 - Dec,2016"} 
                 jobDescription1 = {"Develop and maintain windows platform for the server as a host, connected with multiple clients (other computers)"}
                 jobDescription2 = {"Maintain and support multi-site windows environment along with hardware and software configuration."}
                 jobDescription3 = {"Mobile and Game Development "}
                 />

            </div>

        </div>
    )
}