import React from 'react';
import SingleSkill from './singleSkill';
import './stylesheets/skills.css';

const skillList = [ {"skillName" : "CI/CD", "skillLevel" : "85%"}, {"skillName" : "ACtive Directory", "skillLevel" : "80%"}, {"skillName" : "windows server update services", "skillLevel" : "90%"},{"skillName" : "Powershell", "skillLevel" : "40%"},
                    {"skillName" : "RDP", "skillLevel" : "70%"} ,{"skillName" : "web servers", "skillLevel" : "60%"}, {"skillName" : "Microsoft Azure", "skillLevel" : "70%"}, {"skillName" : "Amazon Web Services", "skillLevel" : "70%"}, 
                    {"skillName" : "Unity", "skillLevel" : "90%"}, {"skillName" : "Android Native", "skillLevel" : "90%"},{"skillName" : "ARDS", "skillLevel" : "70%"}, {"skillName" : "DHCP", "skillLevel" : "70%"}, 
                    {"skillName" : "Firewalls", "skillLevel" : "50%"},{"skillName" : "VPC/Vnet", "skillLevel" : "45%"}, {"skillName" : "F5 Load Balancer", "skillLevel" : "65%"}, {"skillName" : "Office 365", "skillLevel" : "50%"}];

export default function Skills(){
    return(
        <div className="container">
                <SingleSkill skillList = {skillList} />
        </div>
    )
}