import React from 'react';
import ReactWOW from 'react-wow'
import '../fonts/fontface.css'
import './stylesheets/reset.css';
import './stylesheets/about-me.css';

export default function AboutMe(){
    return(
        <div className="container">
            <div className="about-me">

                <div className="col-12 about-me-intro">

                    <ReactWOW animation ='rotateInUpRight'>
                    <h1>
                        Hey! I'm <span>Muhmmad Noon</span>
                    </h1>
                    </ReactWOW>

                    <p className="text-justify">
                    Mobile App & Game Developer from Lahore, Pakistan.
                    Currently employed by Wangard Int. where I work as a <span>Mobile / Game developer as well as System Admin</span>.
                    I have rich experience in developing <span> Front-end systems and writing application code</span>. 
                    Working with command lines, writing servers and modules interest me. I aspire to become a full
                    stack developer in near future. I am a big <span>Sci-Fi</span> junkie .  
                    </p>

                </div>
            </div>
        </div>
    )
}