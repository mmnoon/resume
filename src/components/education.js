import React from 'react';
import SingleEducation from './singleEducation';
import './stylesheets/reset.css';
import './stylesheets/education.css';

export default function Education(){
    return(
        <div className="container row">

            <SingleEducation 
                additionalClass = {"col-lg-10 col-12"}
                instituteName = {"Hajvary University"} 
                duration = {"2014 - 2018"} 
                education = {"Bachelor of Science"} 
                courseName = {"Department of Computer Science."} 
                CGPA ={"2.6"} />

            <SingleEducation 
                additionalClass = {"col-lg-8 col-12"}
                instituteName = {"WESTCoast University"} 
                duration = {"2007 - 2010"} 
                education = {"Bachelor of Arst"} 
                courseName = {"Business Administration"} 
                CGPA ={"3.0"} />

            <SingleEducation 
                additionalClass = {"col-lg-6 col-12"}
                instituteName = {"I.C.B.T"} 
                duration = {"2005 - 2007"} 
                education = {"High School Diploma"} 
                courseName = {"International Business Management Ewarded by Edexcel."} 
                CGPA ={"Pass"} />

        </div>
    )
}